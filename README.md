# Taj : Nextjs starter kit

![eslint sample](src/static/images/logo.svg)

# Introduction

# Getting Started

## Basic Commands

```
yarn start / npm start       - creates a build in /src/out folder and runs the locally
yarn dev / npm run dev       - run the development mode
yarn export / npm run export - create the static HTML files in /src/out
```

## Concepts about project

### SASS

As a feature based file structure all the SCSS files are located at their respective folders.

```
// Component
/featureName
  /index.js
  /index.scss

```

### UI Structure & Framework

#### Grommet UI

we use grommet v2 as primary UI base framework. it has awesome features such as highly customisable flexbox layouting, core components, accessibility support

### Code Linting

#### Prettier

Run Command `yarn pretty`

###### Command Options

--staged (only git)
--pattern (Ex: `pretty-quick --pattern "**/*.*(js|jsx)")`

#### EsLint

![eslint sample](images/eslint.png)

###### eslint a11y plugin

https://github.com/evcohen/eslint-plugin-jsx-a11y

### WCAG 2.0 AA compliance

#### cra-e2e-accessibility

Example application to show:

-   how to do automatic end to end testing with CRA
-   how to include accessibility in those tests.

Videos
https://www.youtube.com/watch?v=jC_7NnRdYb0
https://www.youtube.com/watch?v=YMeCXYrj6l4

#### A11y Setup

`yarn add a11y`
`a11y <url> --delay=5`

#### Axe with Chromedriver for render testing

```
brew tap homebrew/cask
brew cask install chromedriver
```

run command line

`node src/tests/chrome/index`

#### Axe with puppeteer

Run `yarn puppet`

https://gist.github.com/bjankord/c8afaf345b4499ca3b1267063ce48562

#### Pre-requisites

Make sure you have Java installed as Selenium needs it. See https://github.com/vvo/selenium-standalone#ensure-you-have-the-minimum-required-java-version

#### Development

Run `yarn` to install dependencies.

Run `yarn build && yarn test-e2e` to run the tests.

#### Working with API

lets say you have a sample get request `SAMPLE_URL` in `/clerk` page

1. Add the `SAMPLE_URL` in `.env` & `.env.production` files respectively
2. Define `SAMPLE_URL` in `/constants/configStatic`
3. Create file or function necessarily in `/api/` or `/api/clerk/` (use getRequest or postRequest) from `apiUtil` in `/services/util/apiUtil`
4. Create reducers and actions as necessary & import `/api/clerk/` in `actions/clerk/` then use it

#### TypeScript

##### Define Interfaces for your classes

You need to export the interface from the file in which is defined and import it wherever you want to use it.

in `IfcSampleInterface.ts:`

```
export interface IfcSampleInterface {
   key: string;
   value: string;
}
```

In `SampleInterface.ts`

```
import { IfcSampleInterface } from './IfcSampleInterface';
let sampleVar: IfcSampleInterface;
```

Run Command `yarn typecheck`

### Testing

-   Unit Tests
-   Integration Tests
-   UI Tests- (A.K.A Functional Tests)

Read More about testing types in [here](https://medium.com/welldone-software/an-overview-of-javascript-testing-in-2018-f68950900bc3)

#### Commands

-   `yarn test` for local testing
-   There's is a command in pre-commit hook, that runs in lint-staged with Node variable `PRECOMMIT=true`

### Environment Variables

dotenv package is used to manage the keys and Environment Variables. The pattern is as follows

**In this Kit**
`.env` & `.env.production` are the files used at development and build time

<details>
<summary>
<i>Patterns (expand) </i>
</summary>

```
.env: Default.
.env.local: Local overrides. This file is loaded for all environments except test.
.env.development, .env.test, .env.production: Environment-specific settings.
.env.development.local, .env.test.local, .env.production.local: Local overrides of environment-specific settings.
```

Files on the left have more priority than files on the right:

```
npm start: .env.development.local, .env.development, .env.local, .env
npm run build: .env.production.local, .env.production, .env.local, .env
npm test: .env.test.local, .env.test, .env (note .env.local is missing)
```

</details>

#### Usage in Code

<details>
<summary>
  <i>See Code</i>
</summary>
Anything accessible to both client and server-side code should be under publicRuntimeConfig.

next.config.js

```
module.exports = {
  serverRuntimeConfig: { // Will only be available on the server side
    mySecret: 'secret',
    secondSecret: process.env.SECOND_SECRET // Pass through env variables
  },
  publicRuntimeConfig: { // Will be available on both server and client
    staticFolder: '/static',
  }
}
```

pages/index.js

```
import getConfig from 'next/config'
// Only holds serverRuntimeConfig and publicRuntimeConfig from next.config.js nothing else.
const {serverRuntimeConfig, publicRuntimeConfig} = getConfig()

function MyImage() {
  return (
    <div>
      <img src={`${publicRuntimeConfig.staticFolder}/logo.png`} alt="logo" />
    </div>
  )
}

export default MyImage
```

</details>

### Redux

We use few redux & redux libraries and there is a specification for the redux component development.

#### Redux Persist

-   Do not forget to `whitelist` variables in `src/constants/configStatic` -> `persistConfig`

### React Hooks

I strongly recommend to use react hooks for your next project. React Hooks go really well with React Functional Components and Typescript.

#### Refer

-   useContext
-   useRef
-   useState

### Build Management

`husky` for pre-committing code

### Storybook setup

To generate a Storybook, install getstorybook globally

`npm i -g getstorybook`

Then navigate to your project and run

`getstorybook`

### Help / Documentation

This project uses docsify to generate `Read.me` to static site

### Console Helpers

<details>
<summary>
<i>Use Console Log helpers for color texts for easy identification </i>
</summary>
test
</details>

### Async Functions

More detail on async Functions [here](https://developers.google.com/web/fundamentals/primers/async-functions)

### Babel .babelrc vs babel.config.js

More info [here](https://babeljs.io/docs/en/configuration)

### Caveats

#### NextJS `publicRuntimeConfig` issue

`publicRuntimeConfig` is being rendered in exported HTML files. This shouldn't be the way to load the props.

#### TypeScript Module Aliases

https://decembersoft.com/posts/say-goodbye-to-relative-paths-in-typescript-imports/

#### Jest Helpers

http://blogs.quovantis.com/testing-react-components-using-jest-and-enzyme/

#### .bashrc

Used to run any platform (windows & unix based) to run few setups like setting up sshagent with necessary keys

### multiple .env include

Help (https://github.com/motdotla/dotenv/issues/256#issuecomment-359116714)

```
    const dotenv = require('dotenv')
    dotenv.config({ path: 'prod.env' })
    dotenv.config({ path: 'prod.secret.env' })
```

### forwarding Ref for dynamic components

Reference
https://github.com/zeit/next.js/issues/4957#issuecomment-413841689
https://github.com/juancampa/next-bug/blob/master/pages/index.js
