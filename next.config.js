const withTypescript = require('@zeit/next-typescript');
const withSass = require('@zeit/next-sass');
const withCSS = require('@zeit/next-css');
const dotenv = require('dotenv');

/**
 * Environment variables based on the NODE_ENV = dev / test / prod
 */
if (process.env && process.env.NODE_ENV) {
    dotenv.config({ path: `.env.${process.env.NODE_ENV}` });
    dotenv.config({ path: `.env.urls` });
} else {
    dotenv.config({ path: `.env` });
    dotenv.config({ path: `.env.urls` });
}

/**
 * Grommet UI Modules include
 */
const withTM = require('next-plugin-transpile-modules');
const initExport = {
    transpileModules: ['grommet-controls', 'grommet', 'grommet-icons']
};
module.exports = withTM(initExport);

/**
 * Config
 */
const path = require('path');
const publicConfig = require('./src/constants/');
// import publicConfig from './src/constants/nextConfigProvider';

const { publicRuntimeConfig } = publicConfig;

/**
 * Use routes defined in the
 */
const exportPathMapFunc = async () => {
    const pathsObj = Object.assign({}, {}, publicRuntimeConfig.routes);
    return pathsObj;
};

module.exports = withTypescript(
    withCSS(
        withSass({
            exportPathMap: () => {
                return exportPathMapFunc();
            },
            outDir: `out_${process.env.NODE_ENV}`,
            publicRuntimeConfig: {},
            serverRuntimeConfig: {
                /**
                 *  Server side only config
                 */
                mySecret: 'secret'
            },

            webpack(cfg) {
                const originalEntry = cfg.entry;
                cfg.entry = async () => {
                    const entries = await originalEntry();

                    /**
                     * To support cross browser polyfills
                     */
                    if (
                        entries['main.js'] &&
                        !entries['main.js'].includes(
                            path.resolve('./client/polyfills.js')
                        )
                    ) {
                        entries['main.js'].unshift(
                            path.resolve('./client/polyfills.js')
                        );
                    }

                    return entries;
                };

                return cfg;
            }
        })
    )
);
