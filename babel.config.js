module.exports = function(api) {
    api.cache(true);

    const presets = ['next/babel', '@zeit/next-typescript/babel'];
    const plugins = [
        'styled-components',
        'babel-plugin-transform-inline-environment-variables',
        [
            'module-resolver',
            {
                alias: {
                    Actions: './src/services/actions',
                    Api: './src/services/api/index',
                    Constants: './src/constants',
                    Services: './src/services/api/index',
                    Util: './src/services/util'
                },
                root: ['']
            }
        ]
    ];

    /*
      Check if the environment is NODE_ENV == test
    */
    if (process.env.NODE_ENV === 'test' && process.env.PRECOMMIT === true) {
        const env = {
            test: {
                presets: [['env', { modules: 'commonjs' }]]
            }
        };
        return {
            env,
            plugins,
            presets
        };
    }

    /*
        else return just presets and plugins
     */
    return {
        plugins,
        presets
    };
};
