// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
    roots: ['src/'],
    testPathIgnorePatterns: [
        'src/out',
        'src/testsOutput',
        'src/static',
        'src/.next'
    ],

    // Automatically clear mock calls and instances between every test
    clearMocks: true,
    setupFiles: ['./enzyme.config.js', './jest.setup.js'],
    testResultsProcessor: './src/tests/resultProcessor.js'
};
