/** Used in jest.config.js */
const enzyme = require('enzyme');

const {
    // shallow, render, mount,
    configure
} = enzyme;
const Adapter = require('enzyme-adapter-react-16');

configure({ adapter: new Adapter() });

// global.shallow = shallow;
// global.render = render;
// global.mount = mount;
