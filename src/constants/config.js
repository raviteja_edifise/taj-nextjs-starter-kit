/**
 *  Application Config
 *  ------------------------------------------------------------------
 *  Will be available on both server and client
 */
const Routes = require('./routes');

module.exports = {
    API_BASE_URL: process.env.API_BASE_URL,

    UITEST_URL:
        process.env.NODE_ENV === 'test' &&
        (process.env.LOCALTEST || process.env.PRECOMMIT)
            ? process.env.LOCAL_URL
            : process.env.SERVER_URL,

    persistConfig: ['Login', 'ref'],

    api: Routes.api,
    pathnames: Routes.pathnames,
    routes: Routes.routes
};
