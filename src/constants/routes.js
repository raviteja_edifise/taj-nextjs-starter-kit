/*
  Sample object

  urlsdata = {
    featurename : {
      routes: {
        'routename' :  { page: '/value' } // (in code ex: publicRuntimeConfig.api.routename)
        ...
      },
      api: {
        path: 'value'
        ...
      }
    } // end of feature setting

  }

*/

const extend = (obj, src) => {
    for (const key of Object.keys(src)) {
        if (src[key]) {
            obj[key] = src[key];
        }
    }
    return obj;
};

const mapRoutes = urlsdata => {
    let routes = {};
    let api = {};
    let pathnames = {};

    for (const key of Object.keys(urlsdata)) {
        if (urlsdata[key]) {
            if (urlsdata[key].routes) {
                const obj = urlsdata[key].routes;

                if (urlsdata[key].disabled) {
                    for (const subkey of Object.keys(obj)) {
                        if (obj[subkey]) {
                            obj[subkey].disabled = true;
                        }
                    }
                }

                if (urlsdata[key].disableSessionCheck) {
                    for (const subkey of Object.keys(obj)) {
                        if (obj[subkey] && typeof obj[subkey] === 'object') {
                            obj[subkey].disableSessionCheck = true;
                        }
                    }
                }

                if (urlsdata[key].userRoleAsDefined) {
                    for (const subkey of Object.keys(obj)) {
                        if (obj[subkey]) {
                            obj[subkey].roles = urlsdata[key].userRoleAsDefined;
                        }
                    }
                }

                routes = extend(obj, routes);
            }

            if (urlsdata[key].api) {
                const obj2 = urlsdata[key].api;
                /*
            if disabled, then ignore the api URLS to enter to the
            */
                // if(!urlsdata[key].disabled)
                // api = extend(obj2, api);
                if (urlsdata[key].useKeyInAPI) {
                    api[key] = obj2;
                }
                // use the keyname in API
                else {
                    api = extend(obj2, api);
                } // else add the api urls to api object
            }

            if (urlsdata[key].pathnames) {
                const obj3 = urlsdata[key].pathnames;
                /*
            if disabled, then ignore the api URLS to enter to the
            */
                // if(!urlsdata[key].disabled)
                pathnames = extend(obj3, pathnames);
            }
        }
    }

    return {
        api,
        pathnames,
        routes
    };
};
// const RolesTypes = require('./RolesTypes.ts');

const urls = {
    forgot: {
        disableSessionCheck: true,
        pathnames: {
            forgot: '/forgot'
        },
        routes: {
            '/forgot': { page: '/forgot' }
        }
    },

    other: {
        pathnames: {
            base: process.env.SERVER_URL + '/',
            home: '/home'
        },
        routes: {
            '/': { page: '/' },
            '/home': { page: '/' }
        }
    }
};

const mapped = mapRoutes(urls);
// export default mapped;
module.exports = mapped;
