// import Config from './config';
const Config = require('./config');

const getConfig = () => {
    // Does lots of stuff
    return {
        publicRuntimeConfig: Config
    };
};

module.exports = {
    getConfig: getConfig,
    publicRuntimeConfig: Config
};
