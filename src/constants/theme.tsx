const theme = {
    button: {
        border: {
            radius: 0
        },
        extend: props => `
      font-weight: 500;
      text-transform: uppercase;
      font-size: 14px;

      ${props && props.primary && 'color: white;'}
    `,
        padding: {
            horizontal: '24px',
            vertical: '6px'
        }
    },

    global: {
        colors: {
            blue: '#227093',
            brand: '#1b7bbf',
            cream: '#f7f1e3',
            green: '#218c74',
            orange: '#ff6a00',
            pink: '#ad3e51',
            purple: '#40407a'
        },
        elevation: {
            light: {
                large:
                    '0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)',
                medium:
                    '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)',
                small: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
                xlarge:
                    '0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)',
                xsmall: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)'
            }
        },
        font: {
            family: "'Roboto', Arial, sans-serif",
            size: '14px'
        },
        input: {
            weight: 500
        }
    },

    formField: {
        border: {
            position: 'outer',
            side: 'all'
        },
        label: {
            color: 'dark-4',
            size: 'small',
            weight: 600
        }
    },
    heading: {
        font: {
            family: "'Roboto', Arial, sans-serif"
        }
    },
    select: {
        icons: {
            color: 'dark-5'
        }
    }
};

export { theme };
