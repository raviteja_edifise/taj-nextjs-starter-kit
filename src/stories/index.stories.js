import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';

import { Button, Welcome } from '@storybook/react/demo';

class Ravi extends React.Component {
    render() {
        const { name } = this.props;
        return (
            <div style={{ padding: 20, background: 'red' }}>testing {name}</div>
        );
    }
}

const stories = storiesOf('Welcome', module);
stories.addDecorator(withKnobs);

stories.add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

stories
    .add('with text', () => (
        <Button onClick={action('clicked')} style={{ padding: number(10) }}>
            {' '}
            ss Button
        </Button>
    ))
    .add('with some emoji', () => (
        <Button onClick={action('clicked')}>
            <span role="img" aria-label="so cool">
                😀 😎 👍 💯 something
            </span>
        </Button>
    ))
    .add('Custom', () => {
        const name = text('Name', 'Arunoda Susiripala');
        return <Ravi name={name} disabled={boolean('Disabled', false)} />;
    });
