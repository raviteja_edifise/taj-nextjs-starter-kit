import * as actionTypes from '../../actionTypes/_init';

export function storeLoaded() {
    return {
        payload: true,
        type: actionTypes.STORE_LOADED
    };
}
