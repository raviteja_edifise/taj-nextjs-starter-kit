// import axios from 'axios';
import * as actionTypes from '../../actionTypes/login';

export const loginWithToken = tokens => {
    return dispatch => {
        dispatch({ type: actionTypes.ADD_MSAL_TOKEN, payload: tokens });
        dispatch({ type: actionTypes.ADD_TOKEN_PROCESSING, payload: true });

        /**
         * Manual core domain
         * -------- DO NOT REMOVE THIS BLOCK -----------------
         */

        // // const token = 'YOUR_STATIC_TOKEN_TO_FORWARD_TO_CORE_DOMAIN';
        // const decoded = jwt_decode(tokens.msaltoken);
        // dispatch({
        //     payload: token || null,
        //     sessionInfo: decoded,
        //     type: actionTypes.ADD_ACCESS_TOKEN
        //     // payload: {
        //     //   name: 'ravi',
        //     //   message: 'you are in buddy',
        //     // },
        // });
        // if (callback) {
        //     callback(decoded);
        // }
        /**
         * End core domain
         * -------- DO NOT REMOVE THIS BLOCK END -----------------
         */
    };
};

export const logout = () => {
    return {
        payload: new Promise(resolve => {
            resolve('loggedout');
        }),
        type: actionTypes.LOGOUT
    };
};
