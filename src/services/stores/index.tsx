import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
// import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import { storeLoaded } from '../actions/_init';
import rootReducer from '../reducers/index';

// import { publicRuntimeConfig } from 'Util/';
import { publicRuntimeConfig } from '../util';
import SessionSingleton from '../util/sessionSingleton';

interface ICustomStore {
    __persistor?: any;
    dispatch?: any;
    getState?: any;
    replaceReducer?: any;
    subscribe?: any;
}

const makeConfiguredStore = (reducer, initialState) =>
    createStore(
        reducer,
        initialState,
        composeWithDevTools(
            applyMiddleware(
                thunkMiddleware
                // createLogger()
            )
        )
    );

export const initStore = (initialState, { isServer }) => {
    if (isServer) {
        initialState = initialState || {};
        return makeConfiguredStore(rootReducer, initialState);
    }

    // we need it only on client side
    const { persistStore, persistReducer } = require('redux-persist');
    const storage = require('redux-persist/lib/storage').default;

    const persistConfig = {
        key: 'adfhsc',
        storage,
        whitelist: publicRuntimeConfig.persistConfig
    };

    const persistedReducer = persistReducer(persistConfig, rootReducer);
    const store: any = makeConfiguredStore(persistedReducer, initialState);

    store.__persistor = persistStore(store, {}, () => {
        store.dispatch(storeLoaded());
    });

    /**
     * Globally stored in the session Singleton, handle the persistor object there
     */
    const sessionSingleton = SessionSingleton;
    sessionSingleton.setPersistor(store.__persistor);

    return store;
};

/**
 *  this is a direct fetch from localstorage ( this has side effects, the json stored in local storage will not get parsed to JS object when returned )
 *  -- IMPORTANT --
 *  This is intended for login purposes only, So the result returned is only Login object
 *
 *  To properly read the store of redux from local storage ( work with persistor object when you initStore. See https://github.com/rt2zz/redux-persist#persistor-object)
 *
 */
export const fetchLoginfromLocalStorage = () => {
    const getLocal = localStorage['persist:adfhsc'];
    const parsedLogin = JSON.parse(getLocal).Login;
    const parsedSession = JSON.parse(parsedLogin);
    return {
        getState: () => {
            return {
                Login: parsedSession
            };
        }
    };
};

export const fetchExistingStore = async () => {
    return new Promise(resolve => {
        const { persistStore, persistReducer } = require('redux-persist');
        const storage = require('redux-persist/lib/storage').default;

        const persistConfig = {
            key: 'adfhsc',
            storage,
            whitelist: publicRuntimeConfig.persistConfig
        };

        const persistedReducer = persistReducer(persistConfig, rootReducer);
        const store = makeConfiguredStore(persistedReducer, {}) as ICustomStore;

        store.__persistor = persistStore(store, {}, () => {
            resolve(store);
        });

        // return store;
    });
};
