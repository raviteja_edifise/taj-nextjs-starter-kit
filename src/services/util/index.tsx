/**
 * Created by Ravi (ravi.teja.kesanam@avanade.com) on 23/9/17.
 */

const validateEmail = email => {
    const re = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
    return re.test(String(email).toLowerCase());
};

// const chalk = require('chalk');

const { log } = console;

let Router = null;
let getConfig = null;
let publicRuntimeConfig = null;
let _ = null;
let queryString = null;
let is = null;
// let Alert = null;

try {
    Router = require('next/router').default;
    getConfig = require('Constants/');
    publicRuntimeConfig = getConfig.publicRuntimeConfig;
    _ = require('lodash');
    queryString = require('query-string');
    is = require('is_js');
} catch (e) {
    // console.error(' Router Module Load Error ');
}

const redirectToPage = PageName => {
    Router.push(PageName);
};

const redirectToDashboardPage = () => {
    Router.push(publicRuntimeConfig.pathnames.home);
};

const sanitize = value => {
    // const clean = DOMPurify.sanitize(value);
    return value;
};
const getQueryParam = name => {
    if (!queryString) {
        return null;
    }
    const parsed = queryString.parse(window.location.search);
    if (parsed) {
        if (parsed[name]) {
            return parsed[name];
        } else {
            return null;
        }
    }
};

const formatDateFromUTC = datetimestring => {
    if (!datetimestring) {
        return '';
    }

    return new Date(datetimestring.substr(0, 10)).toLocaleDateString('en-AU', {
        day: 'numeric',
        month: 'short',
        year: 'numeric'
    });
};

const openFileInNewTab = url => {
    const win = window.open(url, '_blank');
    win.focus();
};

const openNewTab = url => {
    if (is && (is.edge() || is.ie())) {
        if (is.edge()) {
            const win2 = window.open(url, 'Download', 'height=200,width=200');
            win2.focus();
            return;
        }

        const win = window.open(url);
        if (!win || win.closed || typeof win.closed === undefined) {
            alert(' Popup blocked !');
        }
        win.focus();
    } else {
        const win = window.open(url, '_blank');
        win.focus();
    }
};

const getFileNameFromDisposition = disposition => {
    let filename = null;
    if (disposition && disposition.indexOf('attachment') !== -1) {
        const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        const matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
            filename = matches[1].replace(/['"]/g, '');
        }
    }
    return filename;
};

const empty = data => {
    return is.all.empty(data);
};

const cleanArray = actual => {
    const newArray = new Array();
    for (const each of actual) {
        if (is.existy(each)) {
            newArray.push(each);
        }
    }
    return newArray;
};

const cleanObj = actual => {
    for (const member in actual) {
        if (!is.existy(actual[member])) {
            delete actual[member];
        }
    }
    return actual;
};
const findInArrOfObjByKey = (arr, key, value) => {
    for (let i = 0, iLen = arr.length; i < iLen; i++) {
        if (arr[i][key] === value) {
            return arr[i];
        }
    }
};

const formTextValidation = value => {
    if (is.empty(value)) {
        return 'cannot be empty';
    }

    if (!is.string(value)) {
        // this case triggers could happen for only non-text fields (like numeric, boolean etc)
        return 'only alphabets with spaces';
    }

    if (!is.alphaNumeric(value.replace(/ /g, ''))) {
        return 'only alphabets with spaces';
    }

    if (value.replace(/ /g, '') < 3) {
        return 'minimum 3 characters';
    }

    if (value.replace(/ /g, '') > 50) {
        return 'maximum 50 characters';
    }
    return null;
};

const validateText = ({
    value,
    min = 3,
    max = 50,
    alphaNumeric = false,
    required = false
}) => {
    const input = value ? value.replace(/ /g, '') : '';

    if (required && is.empty(input)) {
        return 'cannot be empty';
    }

    if (alphaNumeric) {
        if (!is.alphaNumeric(input)) {
            return 'only alphabets with spaces';
        }
    }

    if (input.length < min) {
        return 'minimum ' + min + ' characters';
    }

    if (input.length > max) {
        return 'maximum ' + max + ' characters';
    }
    return null;
};

const formEmailValidation = value => {
    if (is.empty(value)) {
        return 'cannot be empty';
    }

    if (!is.email(value)) {
        // this case triggers could happen for only non-text fields (like numeric, boolean etc)
        return 'not a valid e-mail';
    }

    return null;
};

const formPhoneValidation = value => {
    if (is.empty(value)) {
        return 'cannot be empty';
    }

    if (!is.nanpPhone(value)) {
        // this case triggers could happen for only non-text fields (like numeric, boolean etc)

        return 'not a valid phone number';
    }

    return null;
};

// const formTextAreaValidation = ({value, required = false, specialChars = false, min, max = 500 }) => {
const formTextAreaValidation = value => {
    const required = false;
    const specialChars = false;
    const min = false;
    const max = 500;

    if (required && is.empty(value)) {
        return 'cannot be empty';
    }

    if (value.length > 0 && !specialChars && !is.string(value)) {
        // this case triggers could happen for only non-text fields (like numeric, boolean etc)
        return 'no special characters';
    }

    if (
        value.length > 0 &&
        !specialChars &&
        !is.alphaNumeric(value.replace(/ /g, ''))
    ) {
        return 'no special characters';
    }

    if (
        value.length > 0 &&
        min &&
        is.number(min) &&
        value.replace(/ /g, '') < min
    ) {
        return 'minimum ' + min + ' characters';
    }

    if (
        value.length > 0 &&
        max &&
        is.number(max) &&
        value.replace(/ /g, '').length > max
    ) {
        return 'maximum ' + max + ' characters';
    }

    return null;
};

const scrollTo = element => {
    if (!element) {
        return;
    }
    // element.focus();
    if (!window) {
        return;
    }
    window.scrollTo({
        behavior: 'smooth',
        top: element.offsetTop
    });
};

const util = {
    _,
    cleanArray,
    cleanObj,
    empty,
    findInArrOfObjByKey,
    formEmailValidation,
    formPhoneValidation,
    formTextAreaValidation,
    formTextValidation,
    formatDateFromUTC,
    getFileNameFromDisposition,
    getQueryParam,
    log,
    openFileInNewTab,
    openNewTab,
    publicRuntimeConfig,
    redirectToDashboardPage,
    redirectToPage,
    sanitize,
    scrollTo,
    validateEmail,
    validateText
};

export {
    _,
    cleanArray,
    cleanObj,
    empty,
    findInArrOfObjByKey,
    formEmailValidation,
    formPhoneValidation,
    formTextAreaValidation,
    formTextValidation,
    formatDateFromUTC,
    getFileNameFromDisposition,
    getQueryParam,
    is,
    log,
    openFileInNewTab,
    openNewTab,
    publicRuntimeConfig,
    redirectToDashboardPage,
    redirectToPage,
    sanitize,
    scrollTo,
    validateEmail,
    validateText
};

module.exports = util;
