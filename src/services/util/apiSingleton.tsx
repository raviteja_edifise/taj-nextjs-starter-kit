import axios from 'axios';

const singleton = Symbol();
const singletonEnforcer = Symbol();

class ApiSingleton {
    public session: any;
    private isAuthorisationSet: boolean;

    constructor(enforcer) {
        if (enforcer !== singletonEnforcer) {
            throw new Error('Cannot construct singleton');
        }

        this.session = null;
        this.isAuthorisationSet = false;
    }

    static get instance() {
        // Try to get an efficient singleton
        if (!this[singleton]) {
            this[singleton] = new ApiSingleton(singletonEnforcer);
        }

        return this[singleton];
    }

    public isAuthorisationReady = () => {
        return this.isAuthorisationSet;
    };

    public setAuthorisation = () => {
        this.isAuthorisationSet = true;

        this.session = axios.create({
            // baseURL: publicRuntimeConfig.API_BASE_URL,
            headers: {
                // Authorization: `Bearer ${token}`,
                // 'Cache-Control': 'no-cache, no-store, must-revalidate',
                // Expires: 0,
                // Pragma: 'no-cache'
            }
        });

        this.session.interceptors.request.use(request => {
            /**
             * Co-relate API calls
             */
            // request.headers['X-Correlation-Id'] = UUID;

            return request;
        });

        this.session.interceptors.response.use(
            response => {
                /**
                 * Track complete response
                 */
                return response;
            },
            error => {
                /**
                 * Track error response
                 */
                return Promise.reject(error);
            }
        );
    };

    public getWithOutHeader = (url, options) => {
        return axios.get(url, options);
    };

    public get = (url, options) => {
        return this.session.get(url, options);
    };
    public post = (url, data, options) => {
        return this.session.post(url, data, options);
    };
    public put = (url, data, options) => {
        return this.session.put(url, data, options);
    };

    public delete = (url, data) => this.session.delete(url, data);
}

export default ApiSingleton.instance;
