declare module 'Util' {
    function getQueryParam(name: string);
    function logger();
    function redirectToPage(PageName: string);
    function validateEmail(email: string);
    function publicRuntimeConfig();
}
