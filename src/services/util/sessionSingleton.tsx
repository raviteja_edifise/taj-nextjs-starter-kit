const sessionSingleton = Symbol();
const sessionSingletonEnforcer = Symbol();

class SessionInfoSingleton {
    public session: any;
    private isSessionSet: boolean;

    private persistor: any;

    constructor(enforcer) {
        if (enforcer !== sessionSingletonEnforcer) {
            throw new Error('Cannot construct sessionInfo');
        }

        this.session = null;
        this.isSessionSet = false;
        this.persistor = null;
    }

    static get instance() {
        // Try to get an efficient singleton
        if (!this[sessionSingleton]) {
            this[sessionSingleton] = new SessionInfoSingleton(
                sessionSingletonEnforcer
            );
        }

        return this[sessionSingleton];
    }

    public isSessionReady = () => {
        return this.isSessionSet;
    };

    public setSession = sessionInfo => {
        this.isSessionSet = true;
        this.session = sessionInfo;
    };

    public setPersistor = persistor => {
        this.persistor = persistor;
    };

    public getPersistor = () => {
        return this.persistor;
    };

    public get = () => {
        return this.session;
    };
}

export default SessionInfoSingleton.instance;
