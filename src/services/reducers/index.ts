import { combineReducers } from 'redux';
import initReducer from './_init';
import LoginReducer from './login';

export default combineReducers({
    Login: LoginReducer,
    pageState: initReducer
});
