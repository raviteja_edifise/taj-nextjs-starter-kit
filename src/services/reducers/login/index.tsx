import * as actionTypes from '../../actionTypes/login';
// import { publicRuntimeConfig } from 'Util/';

const initialState = {
    accesstoken: null,
    // config: publicRuntimeConfig,
    error: null,
    fetched: false,
    fetching: false,
    msaltoken: null,
    sessionInfo: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_MSAL_TOKEN: {
            return {
                ...state,
                msaltoken: action.payload.msaltoken
            };
            // break;
        }
        case actionTypes.ADD_TOKEN_PROCESSING: {
            return { ...state, fetching: true, error: null };
            // break;
        }
        case actionTypes.ADD_ACCESS_TOKEN: {
            return {
                ...state,
                accesstoken: action.payload,
                fetched: true,
                fetching: false,
                sessionInfo: action.sessionInfo
            };
            // break;
        }
        case actionTypes.ADD_TOKEN_ERROR: {
            return {
                ...state,
                accesstoken: null,
                error: action.payload,
                fetching: false,
                msaltoken: null
            };
            // break;
        }
        case actionTypes.LOGOUT: {
            return {
                ...state,
                accesstoken: null,
                error: null,
                fetched: false,
                fetching: false,
                msaltoken: null,
                sessionInfo: null
            };
            // break;
        }
        default:
            break;
    }
    return state;
};

export default reducer;
