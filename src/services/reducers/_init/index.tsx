import * as actionTypes from '../../actionTypes/_init';

const initialState = {
    storeLoaded: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.STORE_LOADED: {
            return {
                ...state,
                storeLoaded: true
            };
            // break;
        }
        default:
            break;
    }
    return state;
};

export default reducer;
