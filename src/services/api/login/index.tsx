import { getRequestWithOutHeader } from '../../util/apiUtil';

// import { publicRuntimeConfig } from 'Util/';
import { publicRuntimeConfig } from '../../util';

export const getAccessToken = accessToken => {
    return getRequestWithOutHeader(publicRuntimeConfig.api.getAccessToken, {
        headers: {
            Authorization: `Bearer ${accessToken}`,
            'Ocp-Apim-Subscription-Key': publicRuntimeConfig.header_apimkey,
            'Ocp-Apim-Subscription-Key2': publicRuntimeConfig.header_apimkey2
        }
    });
};
