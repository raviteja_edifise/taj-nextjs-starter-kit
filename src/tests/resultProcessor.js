var builder = require('jest-trx-results-processor');

var processor = builder({
    outputFile: 'TEST_RESULTS.trx'
});

module.exports = processor;
