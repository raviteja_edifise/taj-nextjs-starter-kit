/*
Use AxePuppeteer & puppeteer to automate the UI tests

--------------- IMPORTANT ---------------
while using multiple URLs make sure the URLs are local and then execution time should be below 5 secs
or use
```
 beforeEach(() => {
    jest.setTimeout(10000);
  });
```
*/

const { AxePuppeteer } = require('axe-puppeteer');
const AxeReports = require('axe-reports');
const puppeteer = require('puppeteer-core');
const chalk = require('chalk');
const highlight = require('cli-highlight').highlight;
const { log = () => void 0 } = console;
const logjson = obj =>
    log(
        highlight(JSON.stringify(obj, null, 4), {
            ignoreIllegals: true,
            language: 'json'
        })
    );

// import { formatAccessibilityViolations } from '../util/';
// const { formatAccessibilityViolations } = require('../util/');

// log(
//     process.env.NODE_ENV === 'test',
//     process.env.LOCALTEST,
//     process.env.PRECOMMIT
// );

// const config = require('../../constants/nextConfigProvider.ts');
// const { publicRuntimeConfig } = config;
// import getConfig from 'Constants/';
// const { publicRuntimeConfig } = getConfig();

const urls = [
    // 'http://localhost:8080/kitten/',
    // 'https://engineering.cerner.com/terra-ui/#/getting-started/terra-ui/what-is-terra',
    //   publicRuntimeConfig.TEST_URL+'/uitest/',
    'http://localhost:3000/'
    // process.env.NODE_ENV === 'test' && process.env.PRECOMMIT
    //     ? 'http://localhost:3000/uitest/'
];

interface ILaunchOptions {
    executablePath: string;
    headless?: boolean;
}

describe('Home page', () => {
    // let something = " My something ";
    beforeAll(() => {
        jest.setTimeout(20000);
    });

    test('check accessibility issues for URLs', async done => {
        const results = [];
        // let axeResults;
        const promises = [];

        let options = {
            executablePath: 'browser/chrome-win/chrome.exe'
        } as ILaunchOptions;
        if (
            process.env.TESTMAC &&
            process.env.LOCALTEST &&
            process.env.NODE_ENV === 'test'
        ) {
            options = {
                executablePath:
                    '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
                headless: false
            };
            // exePath = '/Applications/Chromium.app/Contents/MacOS/Chromium';
        }

        const browser = await puppeteer.launch(options);

        const analyzePage = async testURL => {
            const page2 = await browser.newPage();
            await page2.setBypassCSP(true);
            await page2.goto(testURL);
            const axeResults = await new AxePuppeteer(page2)
                .withTags(['wcag2a', 'wcag2aa'])
                .analyze();
            // // remove result data we don't need
            delete axeResults.passes;
            delete axeResults.inapplicable;
            delete axeResults.incomplete;

            if (axeResults.violations.length) {
                results.push(axeResults);
            }
            // await page2.screenshot({ path: 'src/testsOutput/'+((new Date()).toLocaleTimeString())+'.png' })
            // await page2.close();

            await page2.close();

            return axeResults;
        };

        for (const url of urls) {
            promises.push(analyzePage(url));
            // promises.push(browser.newPage().then(async page => {
            // await page.goto(`${url}`);
        }

        await Promise.all(promises);
        // let output = await analyzePage();
        await browser.close();

        if (results.length === 0) {
            done();
        } else {
            log(chalk.white.bgRed.bold('    JEST ERROR    '));
            log(
                chalk.white(
                    'one or more supplied URLs have accessibility issues'
                )
            );
            log(
                chalk.whiteBright(
                    '--------------------------------------------------------------------------'
                )
            );

            for (const eachResult of results) {
                AxeReports.processResults(eachResult, 'csv', 'axeReport', true);
                // AxeReports.createCsvReport(eachResult);

                logjson(eachResult.violations);
                // log(formatAccessibilityViolations(eachResult.violations));
            }

            log(
                chalk.whiteBright(
                    '------------------------------- END ---------------------------------------'
                )
            );

            if (results.length) {
                done.fail(new Error(' Page: ' + urls[0] + ' has WCAG errors'));

                // default successful
                // done();
            }
        }

        // throw
    });
});
