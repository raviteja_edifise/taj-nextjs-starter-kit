// import { expect } from 'chai';
import { mount } from 'enzyme';
import React from 'react';
// import Foo from './Foo';
// import { setJestConfig } from '../../util/';
// setJestConfig();

import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

const mockStore = configureMockStore();
const store = mockStore({
    Login: {
        accesstoken: 'something',
        msaltoken: 'testing'
    },
    pageState: {
        storeLoaded: true
    },
    router: {
        pathname: '/uitest'
    }
});

import UITestPage from '../../../pages/index';

describe('<UITestPage />', () => {
    // const mockedEnv = require('mocked-env')
    // let restore
    beforeEach(() => {
        // pass object with modified / added / deleted values
        // it will be merged into existing process.env
        // restore = mockedEnv({
        //   USER: 'test-user',
        //   PWD: undefined, // will be deleted from process.env
        // })
    });

    it('UITest Page render', () => {
        const wrapper = mount(
            <Provider store={store}>
                <UITestPage />
            </Provider>
        );
        // expect(wrapper.exists(<h6>Welcome</h6>)).toBe(false);
        expect(wrapper.contains(<h6>Welcome User</h6>)).toBeTruthy();
    });

    // it('renders the title', () => {
    //   const wrapper = render(<Foo title="unique" />);
    //   expect(wrapper.text()).to.contain('unique');
    // });

    afterEach(() => {
        // don't forget to restore the old process.env
        // restore()
    });
});
