import { Box } from 'grommet';
import Popup from '../Popup';
import { Spinner } from '../Spinner';

const PopUpSpinner = () => (
    <Popup
        content={
            <Box
                animation={['fadeIn']}
                align="center"
                justify="center"
                style={{ width: '80px', height: '80px' }}
            >
                <Spinner />
            </Box>
        }
        // Extend using Layer Props
        // full={true}
        // margin={'100px'}
        position={'center'}
        // onEsc={this.showhidePopup}
    />
);
export default PopUpSpinner;
