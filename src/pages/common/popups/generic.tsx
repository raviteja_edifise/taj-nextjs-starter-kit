import { Box, Button, Text } from 'grommet';
import React from 'react';
import Popup from '../Popup';

export enum PopUpTypes {
    info = 'info',
    error = 'error',
    success = 'success',
    warning = 'warning'
}

enum PopUpImages {
    info = '/static/images/icons/Lightbox_Warning.svg',
    error = '/static/images/icons/Lightbox_Error.svg',
    success = '/static/images/icons/Lightbox_Success.svg',
    warning = '/static/images/icons/Lightbox_Warning.svg'
}

interface IActions {
    label: string;
    onClick: () => void;
    buttonProps?: object;
    render?: React.ReactNode;
}

export default class GenericPopup extends React.Component<{
    header: React.ReactNode;
    content: React.ReactNode; // content is gonna override all your sub content attributes

    type: PopUpTypes; // one of PopUpTypes
    title: string | boolean | React.ReactNode;
    message: string | boolean | React.ReactNode;

    footer: React.ReactNode;
    footerProps: object;
    actions: IActions[];
    // actions: IActions[] | React.ReactNode;
}> {
    public render() {
        const {
            header,
            content,
            footer,
            footerProps = {},
            title = 'Alert',
            message = 'You do not have enough permissions to do this',
            type = PopUpTypes.warning,
            actions = [] as IActions[],
            ...restProps
        } = this.props;
        const position = 'center';

        return (
            <Popup
                header={header || null}
                content={
                    content || (
                        <Box
                            width="medium"
                            pad="medium"
                            flex={true}
                            align="center"
                            justify="center"
                        >
                            <img
                                style={{
                                    margin: '0 auto 50px auto'
                                }}
                                src={PopUpImages[PopUpTypes[type]]}
                                width="140"
                                height="140"
                            />

                            {title && (
                                <Text
                                    weight="bold"
                                    size="large"
                                    style={{ marginBottom: '25px' }}
                                >
                                    {title}
                                </Text>
                            )}
                            {message && <Text size="medium">{message}</Text>}
                        </Box>
                    )
                }
                footer={
                    footer || (
                        <Box
                            as="footer"
                            border={{ side: 'top' }}
                            pad="medium"
                            justify="between"
                            direction="column"
                            align="center"
                            {...footerProps}
                        >
                            {/* <Button
                        label="Close"
                        onClick={close}
                        backgroundColor={theme.global.colors.grey}
                    >
                        Return to page
                    </Button> */}
                            {actions.length > 0 &&
                                actions.map((item, index) => {
                                    if (item.render) {
                                        return item.render;
                                    }

                                    const { buttonProps = {} } = item;
                                    return (
                                        <Button
                                            key={'action_' + index}
                                            label={item.label}
                                            onClick={item.onClick}
                                            {...buttonProps}
                                        >
                                            {item.label}
                                        </Button>
                                    );
                                })}
                        </Box>
                    )
                }
                // Extend using Layer Props
                full={false}
                margin={'100px'}
                position={position}
                {...restProps}
            />
        );
    }
}
