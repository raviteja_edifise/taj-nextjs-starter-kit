import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
const reduxHOC = (ComposedComponent, mapStatetoProps, actionCreators) => {
    class ReduxContainer extends React.PureComponent {
        public static propTypes: {
            dispatch: PropTypes.Requireable<(...args: any[]) => any>;
        };
        public static defaultProps: { dispatch: () => void };
        public boundActionCreators: any;
        constructor(props) {
            super(props);
            const { dispatch } = props;
            this.boundActionCreators = bindActionCreators(
                actionCreators,
                dispatch
            );
        }
        public render() {
            return (
                <ComposedComponent
                    {...this.props}
                    actions={this.boundActionCreators}
                />
            );
        }
    }
    ReduxContainer.propTypes = {
        dispatch: PropTypes.func
    };
    ReduxContainer.defaultProps = {
        dispatch: () => void 0
    };
    return connect(mapStatetoProps)(ReduxContainer);
};

export default reduxHOC;
