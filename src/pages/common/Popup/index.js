import * as React from 'react';
import { Box, Layer } from 'grommet';
// import { Image } from 'react';
import PropTypes from 'prop-types';

export class Popup extends React.Component {
    render() {
        const {
            header,
            content,
            footer,
            custom,
            boxProps,
            ...restProps
        } = this.props;
        return (
            <Layer {...restProps}>
                {/* render custom component or structure header+content+footer */}
                {custom ? (
                    custom
                ) : (
                    <Box {...boxProps}>
                        {header ? header : null}

                        <Box flex overflow="auto" pad="xsmall">
                            {content ? content : null}
                        </Box>

                        {footer ? footer : null}
                    </Box>
                )}
            </Layer>
        );
    }
}

Popup.propTypes = {
    boxProps: PropTypes.object,
    content: PropTypes.node,
    custom: PropTypes.node,
    footer: PropTypes.node,
    header: PropTypes.node
};

Popup.defaultProps = {
    boxProps: {}
};

export default Popup;
