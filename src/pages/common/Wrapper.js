import React from 'react';
import { Grommet } from 'grommet';
import { ThemeContext } from 'styled-components';
import { theme } from '../../constants/theme';

export class Wrapper extends React.Component {
    render() {
        // const { grommet } = this.context;
        const { children } = this.props;

        // theme.global.colors.brand = text('brand Color', theme.global.colors.brand);

        return (
            <Grommet theme={theme}>
                <ThemeContext.Consumer>
                    {() => (
                        <div>
                            {/* {React.cloneElement(children, { ...grommet, ...theme })} */}
                            {children}
                        </div>
                    )}
                </ThemeContext.Consumer>
            </Grommet>
        );
    }
}

export default Wrapper;
