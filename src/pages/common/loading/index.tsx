import { Box, Text } from 'grommet';
import ContentLoader from 'react-content-loader';
import { is } from '../../../services/util/';
import PopUpGeneric from '../popups/generic';
import PopUpSpinner from '../popups/spinner';
const Defaultloader = () => (
    <ContentLoader>
        {/* Only SVG shapes */}
        <rect x="0" y="0" rx="5" ry="5" width="70" height="70" />
        <rect x="80" y="17" rx="4" ry="4" width="300" height="13" />
        <rect x="80" y="40" rx="3" ry="3" width="250" height="10" />
    </ContentLoader>
);

const Container = ({
    loading = true,
    loader = null,
    children = null,
    error = null,
    showSpinner = false,
    // spinner = PopUpSpinner,
    popup = null,
    ...restProps
}) => {
    if (error) {
        return (
            <Box {...restProps}>
                {!!error ? error : <Text color="error">{error}</Text>}
            </Box>
        );
    }

    if (loading) {
        return (
            <Box {...restProps}>{!!loader ? loader : <Defaultloader />}</Box>
        );
    } else {
        return (
            <Box>
                {children}
                {showSpinner && <PopUpSpinner />}

                {is.truthy(popup) && <PopUpGeneric {...popup} />}
            </Box>
        );
    }
};

export default Container;
