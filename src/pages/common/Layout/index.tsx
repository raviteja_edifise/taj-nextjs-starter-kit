// import { Box, Grommet, SkipLinkTarget } from 'grommet';
import { Box } from 'grommet';
import { withRouter } from 'next/router';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { theme } from '../../../constants/theme';
import Wrapper from '../Wrapper';

interface IAppProps {
    pageState?: object;
    router?: object;
}

const mapStateToProps = state => {
    return {
        Login: state.Login,
        pageState: state.pageState
    };
};

const withLayout = BaseComponent => {
    class App extends React.Component<IAppProps, {}> {
        public static propTypes = {
            pageContext: PropTypes.object // eslint-disable-line
        };

        public static defaultProps = {
            // pageContext: null,
        };

        public static getInitialProps = ctx => {
            if (BaseComponent.getInitialProps) {
                return BaseComponent.getInitialProps(ctx);
            }
            return {};
        };

        public componentDidMount() {
            // const { router } = this.props;
            // const { pathname } = router as IRouter;
        }

        public render() {
            return (
                <Wrapper theme={theme}>
                    <Box fill={true}>
                        {/* <Header /> */}

                        <BaseComponent {...this.props} />

                        {/* <Footer justifyContent="center" /> */}
                    </Box>
                </Wrapper>
            );
        }
    }

    // return App;
    return connect(
        mapStateToProps,
        null
    )(withRouter(App as any));
};

export default withLayout;
