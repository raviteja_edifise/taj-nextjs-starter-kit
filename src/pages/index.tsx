import { Box } from 'grommet';
import Head from 'next/head';
import { Component } from 'react';

import withLayout from './common/Layout';

// import './index.scss';
// import './index.css';

class IndexPage extends Component {
    public render() {
        return (
            <main>
                <Head>
                    <title>Project Green</title>
                </Head>

                <Box gridArea="main" fill="vertical" a11yTitle="main content">
                    <Box
                        direction="column"
                        pad="medium"
                        width="large"
                        alignContent="center"
                        alignSelf="center"
                    >
                        <h3>HomePage</h3>
                        <h6>Welcome User</h6>
                    </Box>
                </Box>
            </main>
        );
    }
}

export default withLayout(IndexPage);
