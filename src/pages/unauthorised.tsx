import { Box, Heading } from 'grommet';
import Head from 'next/head';
import { Component } from 'react';

import withLayout from './common/Layout';

class IndexPage extends Component {
    public render() {
        return (
            <main>
                <Head>
                    <title>UnAuthorised</title>
                </Head>

                <Box gridArea="main" fill="vertical" a11yTitle="main content">
                    <Box
                        direction="column"
                        pad="medium"
                        width="large"
                        alignContent="center"
                        alignSelf="center"
                    >
                        <Heading level={3}>
                            You dont have permission to view this page
                        </Heading>
                    </Box>
                </Box>
            </main>
        );
    }
}

export default withLayout(IndexPage);
