import Document, { Head, Main, NextScript } from 'next/document';

import { ServerStyleSheet } from 'styled-components';
// import { theme } from '../constants/theme';

interface INumber {
    [Symbol.iterator](): IterableIterator<string>;
}

class MyDocument extends Document {
    public static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx);

        const sheet = new ServerStyleSheet();
        const originalRenderPage = ctx.renderPage;

        try {
            ctx.renderPage = () =>
                originalRenderPage({
                    enhanceApp: App => props =>
                        sheet.collectStyles(<App {...props} />)
                });

            const customStyles = initialProps.styles as INumber;
            return {
                ...initialProps,
                styles: [...customStyles, ...sheet.getStyleElement()]
            };
        } finally {
            sheet.seal();
        }
    }

    public render() {
        return (
            <html lang="en">
                <Head>
                    {/* <link href="https://fonts.googleapis.com/css?family=Montserrat:100, 200" rel="stylesheet" />
                    <style>{`* { font-family: Montserrat, sans-serif; } `}</style> */}

                    <link rel="stylesheet" href="/static/css/global.css" />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </html>
        );
    }
}

export default MyDocument;
