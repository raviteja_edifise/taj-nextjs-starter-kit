import withRedux from 'next-redux-wrapper';
import App, { Container } from 'next/app';
import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { initStore } from '../services/stores';

interface IMyAppProps {
    store?: any;
}

interface IMyComponentProps {
    getInitialProps?: any;
}

export default withRedux(initStore, { debug: false })(
    class MyApp extends App<IMyAppProps, {}> {
        public static async getInitialProps({ Component, ctx }) {
            const MyComp = Component as IMyComponentProps;

            return {
                pageProps: MyComp.getInitialProps
                    ? await MyComp.getInitialProps(ctx)
                    : {}
            };
        }

        public onBeforeLift = () => {
            // Check if there is already a user token an deterimine this.state.showAuth
        };

        public render() {
            const { Component, pageProps, store } = this.props;

            return (
                <Container>
                    <Provider store={store}>
                        <PersistGate
                            persistor={store.__persistor}
                            loading={<div> page is loading.... </div>}
                            onBeforeLift={this.onBeforeLift}
                        >
                            <Component {...pageProps} />
                        </PersistGate>
                    </Provider>
                </Container>
            );
        }
    }
);
