import { Box, Button, Heading } from 'grommet';
import Head from 'next/head';
import { Fragment, useState } from 'react';
import {
    // getFileNameFromDisposition,
    getQueryParam
    // is
} from '../services/util';
import { Spinner } from './common/Spinner';

// import { downloadDomainsKeys, downloadFileAPIs } from '../constants/Literals';
import useEffectAsync from '../services/util/useEffectAysnc';
import withLayout from './common/Layout';

// const requestHandler: any = async (request, params, onProgress) => {
//     return new Promise((resolve, reject) => {
//         request(params || {}, onProgress, true)
//             .then(response => {
//                 resolve(response);
//             })
//             .catch(e => {
//                 reject(e);
//             });
//     });
// };

// const saveFile = response => {
//     const responseHeaders = response.headers;
//     const contentType = responseHeaders['content-type'];
//     const contentDisposition = '' + responseHeaders['content-disposition'];
//     const filename = getFileNameFromDisposition(contentDisposition);

//     if (filename) {
//         const fileBlob = new Blob([response.data], {
//             type: contentType || 'application/octet-stream'
//         });
//         const url = window.URL.createObjectURL(fileBlob);
//         const link = document.createElement('a');
//         link.href = url;
//         link.setAttribute('download', filename); // or any other extension
//         document.body.appendChild(link);

//         if (is.ie()) {
//             if (navigator.msSaveOrOpenBlob) {
//                 navigator.msSaveOrOpenBlob(fileBlob, filename);
//             }
//         } else if (is.edge()) {
//             window.open(url);
//         } else {
//             link.click();
//         }

//         if (!is.edge() && !is.ie()) {
//             window.close();
//         }
//     }
// };

/**
 *
 * @param props
 * GET PARAMS
 * id = GUID  // as in fileId
 * type = oneof [ 'clinical', 'referral' ]
 */
const DownloadFile = () => {
    const [fileId] = useState(getQueryParam('id') ? getQueryParam('id') : null);
    const [domain] = useState(
        getQueryParam('domain') ? getQueryParam('domain') : null
    );
    const [
        error
        // setError
    ] = useState(null);
    const [
        progress
        // setProgress
    ] = useState(0);

    useEffectAsync(async () => {
        if (fileId && domain) {
            // const fileDownloadAPIReq = downloadFileAPIs[domain]
            //     ? downloadFileAPIs[domain]
            //     : null;
            // if (fileDownloadAPIReq) {
            //     const params = {};
            //     setError(false);
            //     params[downloadDomainsKeys[domain]] = fileId;
            //     const res = await requestHandler(
            //         fileDownloadAPIReq,
            //         {
            //             ...params
            //         },
            //         percentage => {
            //             setProgress(percentage);
            //         }
            //     ).catch(e => {
            //         setError('Error while downloading ' + e.message);
            //     });
            //     // debugger;
            //     setProgress(100);
            //     saveFile(res);
            // }
        }
    }, []);

    return (
        <Fragment>
            <Head>
                <title>Download</title>
            </Head>

            <Box
                style={{ position: 'absolute', height: '100%' }}
                justify="center"
                align="center"
                gridArea="main"
                fill={true}
                a11yTitle="main content"
            >
                {error && <Heading level={5}>{error}</Heading>}

                {!fileId && !domain && (
                    <Heading level={5}>No file or domain specified</Heading>
                )}

                {!error && progress < 100 && (
                    <Box>
                        <Spinner />
                        <Heading level={5}>{progress}% downloaded...</Heading>
                    </Box>
                )}

                {!error && progress === 100 && (
                    <Box>
                        <Heading level={5}>File Downloaded</Heading>
                        <Button
                            label="Close"
                            onClick={() => {
                                if (window) {
                                    window.close();
                                }
                            }}
                        >
                            Close
                        </Button>
                    </Box>
                )}
            </Box>
        </Fragment>
    );
};

export default withLayout(DownloadFile);
